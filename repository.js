'use strict';

const co = require('co');
const fs = require('mz/fs');
const minimatch = require('minimatch');
const path = require('path');
const yaml = require('js-yaml');
const _ = require('lodash');

const debug = require('debug')('repository');
const dataDir = path.join('node_modules', 'etalab-open-software-base-yaml');

// Utility function
var throwNotFound = () => {
  var err = new Error('program not found.');
  err.status = 404;

  throw err;
};

// Cache object
var cache = {};

var getInCache = co.wrap((name, date) => {
  var noDate = (typeof date === 'undefined');

  if (cache[name]) {
    if (noDate || cache[name].date.getTime() > date.getTime()) {
      debug('Hit ' + name + ' in cache.');
      return cache[name].data;
    }
  }

  debug('Misses ' + name + ' in cache.');
});

var putInCache = co.wrap((name, data) => {
  if (!cache[name]) {
    cache[name] = {};
  }

  cache[name].data = data;
  cache[name].date = new Date();
  debug('Stored ' + name + ' in cache.');
});

// Exposed repository
module.exports.get = co.wrap(function *(name) {
  if (typeof name === 'undefined') {
    var programs = yield getInCache('_all');
    if (!programs) {
      programs = (yield fs.readdir(dataDir))
        .filter(filename => (minimatch(filename, '*.+(yaml|yml)')))
        .map(filename => filename.replace(/\.yaml|\.yml$/, ''));

      programs = yield programs.map(this.get);

      yield putInCache('_all', programs);
    }

    return programs;
  }

  var data = yield getInCache(name);
  if (data) {
    return data;
  }

  var programPath = path.join(dataDir, name + '.yaml');

  var stats;
  try {
    stats = yield fs.stat(programPath);
  } catch (e) {
    if (e.code === 'ENOENT') {
      throwNotFound();
    }

    throw e;
  }

  if (!stats.isFile()) {
    throwNotFound();
  }

  try {
    data = _.merge(data,
      yaml.safeLoad(yield fs.readFile(programPath, 'utf8')));
  } catch (err) {
    throw err;
  }

  yield putInCache(name, data);

  return data;
});

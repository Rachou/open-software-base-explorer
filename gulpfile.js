const gulp = require('gulp');
const minifyCss = require('gulp-minify-css');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const webpack = require('webpack-stream');

gulp.task('styles', ['wiredep'], function() {
  gulp.src('static/src/styles/main.scss')
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(minifyCss({compatibility: 'ie8'}))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('static/dist/styles'))
  ;
});

gulp.task('javascripts', function() {
  gulp.src('static/src/js/json-inspector.jsx')
    .pipe(webpack(require('./webpack.config.js')))
    .pipe(gulp.dest('static/dist/js/'))
  ;

  gulp.src('bower_components/bootstrap-sass/assets/javascripts/bootstrap.min.js')
    .pipe(gulp.dest('static/dist/js'))
  ;

  gulp.src('bower_components/jquery/dist/jquery.min.js')
    .pipe(gulp.dest('static/dist/js'))
  ;

  gulp.src('bower_components/selectize/dist/js/standalone/selectize.min.js')
    .pipe(gulp.dest('static/dist/js'))
  ;
});

gulp.task('wiredep', function() {
  var wiredep = require('wiredep').stream;
  return gulp.src('static/src/styles/main.scss')
    .pipe(wiredep())
    .pipe(gulp.dest('static/src/styles'));
});


gulp.task('default', ['styles', 'javascripts']);

gulp.task('watch', ['default'], function() {
  gulp.watch('static/src/styles/**/*.scss', ['styles']);
  gulp.watch('static/src/js/*.jsx', ['javascripts']);
});
